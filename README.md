# AnonVoice
Anonymous voice chat room

Currently live on [voice.multiplayer.uno](https://voice.multiplayer.uno/)

## Server
[AnonVoice Server repo](https://gitlab.com/zipdox/anonvoice-server/)

## To Do
- Text chat
- Microphone selection
- ~~Disconnect button~~
- ~~Peer volume and muting~~
- ~~Peer deafening~~