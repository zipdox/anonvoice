const rtcConfig = {
    iceServers: [
        {urls: 'stun:stun.l.google.com:19302'}
    ]
}

class VoiceClient{
    constructor(room, username, capture, status, userList, chatTable){
        this.socket = new WebSocket('wss://anonvoice.multiplayer.uno:2096');
        const that = this;
        this.keepaliveInterval = setInterval(function(){
            that.socket.send(JSON.stringify({
                type: 'keepalive'
            }));
        }, 20000);
        // this.socket = new WebSocket('ws://localhost:2096');
        const client = this;
        this.socket.onopen = function(){
            connectSound.currentTime = 0;
            if(!connectSound.playing) connectSound.play();
        }
        this.socket.onmessage = function(msg){
            client._handleMsg(msg);
        }
        this.socket.onclose = function(){
            client.close();
        }
        this.room = room;
        this.username = username ? username: 'Anonymous';
        this.peers = {};
        this.capture = capture;
        this.status = status;
        this.userList = userList;
        this.chatTable = chatTable;
    }

    _updateStatusLabel(){
        const numPeers = Object.keys(this.peers).length;
        this.status.textContent = `Connected to ${this.room ? this.room : 'Lobby'} as ${this.username ? this.username : 'Anonymous'} with ${numPeers} ${numPeers != 1 ? 'peers' : 'peer'}`;
    }

    _handleMsg(msg){
        const parsed = JSON.parse(msg.data);
        // console.log(parsed);
        switch(parsed.type){
            case 'id':
                this.id = parsed.id;
                this.socket.send(JSON.stringify({
                    type: 'ready',
                    room: this.room,
                    username: this.username
                }));
                this._updateStatusLabel();
                break;
            case 'newuser':
                this._newUser(parsed.id, parsed.username);
                break;
            case 'candidate':
                if(this.peers[parsed.id] == undefined) return;
                this.peers[parsed.id].rtc.addIceCandidate(new RTCIceCandidate(parsed.candidate));
                break;
            case 'offer':
                this._userOffer(parsed.id, parsed.username, parsed.offer);
                break;
            case 'reconnectoffer':
                this._reconnectOffer(parsed.id, parsed.username, parsed.offer);
                break;
            case 'answer':
                this._userAnswer(parsed.id, parsed.answer);
                break;
            case 'userleft':
                console.log('socket said user left', parsed.id);
                this._userLeft(parsed.id);
                break;
            case 'keepalive':
                console.log('received keepalive');
                break;
        }
    }

    _initRTC(client, id){
        client.peers[id].rtc.oniceconnectionstatechange = function(){
            if(client.peers[id].rtc.iceConnectionState != 'failed' && client.peers[id].rtc.iceConnectionState != 'disconnected') return;
            console.log('webrtc', client.peers[id].rtc.iceConnectionState, id);
            client._reconnect(id);
        }
        client.peers[id].channel = client.peers[id].rtc.createDataChannel('comms', {reliable: true, negotiated: true, id: 0});
        client.peers[id].channel.onmessage = client.peers[id].msghandler;
        client.peers[id].channel.onopen = function(event){
            console.log(id, 'opened', event.target);
            if(client.capture.enabled) client.peers[id].channel.send('{"type": "starttalking"}');
        }

        // media track handlers
        client.peers[id].sender = client.peers[id].rtc.addTrack(client.capture, new MediaStream()); // need the MediaStream because Chromium is retarded
        client.peers[id].rtc.ontrack = function(event){
            console.log('received track', event);        

            client.peers[id].player = document.createElement('audio');
            client.peers[id].player.srcObject = event.streams[0];
            client.peers[id].player.play();

            client._updateStatusLabel();
        }
        client.peers[id].rtc.onicecandidate = function(event){
            client.socket.send(JSON.stringify({
                type: 'candidate',
                id,
                candidate: event.candidate
            }));
        }
    }

    _createUserLabel(peer, username, capture){
        peer.usernameLabel = document.createElement('tr');
        peer.usernameLabel.insertCell().textContent = username;
        peer.volumeSlider = document.createElement('input');
        peer.volumeSlider.min = 0;
        peer.volumeSlider.max = 100;
        peer.volumeSlider.step = 1;
        peer.volumeSlider.value = 100;
        peer.volumeSlider.type = 'range';
        peer.volumeSlider.oninput = function(){
            if(peer.player == undefined) return;
            peer.player.volume = this.value ** 2 / 10000;
        }
        peer.usernameLabel.insertCell().appendChild(peer.volumeSlider);
        const muteContainer = document.createElement('label');
        muteContainer.className = 'mute';
        const muteCheckbox = document.createElement('input');
        muteCheckbox.type = 'checkbox';
        muteCheckbox.onchange = async function(){
            if(peer.sender == undefined) return;
            if(this.checked){
                peer.sender.replaceTrack(null);
            }else{
                peer.sender.replaceTrack(capture);
            }
        }
        muteContainer.appendChild(muteCheckbox);
        muteContainer.appendChild(document.createElement('span'));
        peer.usernameLabel.insertCell().appendChild(muteContainer);
    }

    _initUser(client, id, username){
        client.peers[id] = {rtc: new RTCPeerConnection(rtcConfig), username};

        // user list
        this._createUserLabel(client.peers[id], username, this.capture, this.silence);
        this.userList.appendChild(client.peers[id].usernameLabel);

        // text message handlers
        client.peers[id].msghandler = function(msg){
            let parsed;
            try{
                parsed = JSON.parse(msg.data);
            }catch(err){
                console.error(err);
                return;
            }
            switch(parsed.type){
                case 'goodbye':
                    console.log('peer said goodbye', id);
                    client._userLeft(id);
                    break;
                case 'starttalking':
                    console.log('start', id);
                    client.peers[id].usernameLabel.style.color = 'red';
                    client.peers[id].usernameLabel.style.fontWeight = 'bold';
                    break;
                case 'stoptalking':
                    console.log('stop', id);
                    client.peers[id].usernameLabel.style.color = null;
                    client.peers[id].usernameLabel.style.fontWeight = null;
                    break;
                case 'chat':
                    client._addChatMessage(client.peers[id].username, parsed.message);
                    break;
            }
        }

        client._initRTC(client, id);
    }

    async _newUser(id, username){
        console.log('New user', id, username);
        this._initUser(this, id, username);

        joinSound.currentTime = 0;
        if(!joinSound.playing) joinSound.play();

        const offer = await this.peers[id].rtc.createOffer();
        console.log('Sending offer', offer);
        this.peers[id].rtc.setLocalDescription(offer);
        this.socket.send(JSON.stringify({
            type: 'offer',
            id,
            offer
        }));
    }

    async _userOffer(id, username, offer){
        console.log('Received offer from', id, username, offer);
        this._initUser(this, id, username);

        joinSound.currentTime = 0;
        if(!joinSound.playing) joinSound.play();

        this.peers[id].rtc.setRemoteDescription(new RTCSessionDescription(offer));
        const answer = await this.peers[id].rtc.createAnswer();
        this.peers[id].rtc.setLocalDescription(answer);
        this.socket.send(JSON.stringify({
            type: 'answer',
            id,
            answer
        }));
    }

    async _reconnectOffer(id, username, offer){
        console.log('Received reconnect offer from', id, username, offer);
        if(this.peers[id].rtc == undefined) return;
        this.peers[id].rtc.close();

        this.peers[id].rtc = new RTCPeerConnection(rtcConfig);
        this._initRTC(this, id);

        reconnectSound.currentTime = 0;
        if(!reconnectSound.playing) reconnectSound.play();

        this.peers[id].rtc.setRemoteDescription(new RTCSessionDescription(offer));
        const answer = this.peers[id].rtc.createAnswer();
        this.peers[id].rtc.setLocalDescription(answer);
        this.socket.send(JSON.stringify({
            type: 'answer',
            id,
            answer
        }));
    }

    _userAnswer(id, answer){
        console.log('Receiver answer from', id, answer);
        this.peers[id].rtc.setRemoteDescription(new RTCSessionDescription(answer));
    }

    _userLeft(id){
        if(this.peers[id] == undefined) return;

        leaveSound.currentTime = 0;
        if(!leaveSound.playing) leaveSound.play();

        console.log('user left', id);
        this.peers[id].usernameLabel.remove();
        if(this.peers[id].rtc != undefined) this.peers[id].rtc.close();
        delete this.peers[id];
        this._updateStatusLabel();
    }

    async _reconnect(id){
        console.log('requesting reconnect', id);
        this.peers[id].rtc.close();

        reconnectSound.currentTime = 0;
        if(!reconnectSound.playing) reconnectSound.play();

        this.peers[id].rtc = new RTCPeerConnection(rtcConfig);
        this._initRTC(this, id);
        const offer = await this.peers[id].rtc.createOffer();
        console.log('Sending reconnect offer', offer);
        this.peers[id].rtc.setLocalDescription(offer);
        this.socket.send(JSON.stringify({
            type: 'reconnectoffer',
            id,
            offer
        }));
    }

    startTalking(){
        this.capture.enabled = true;
        for(let id in this.peers){
            if(this.peers[id].channel) this.peers[id].channel.send('{"type": "starttalking"}');
        }
    }

    stopTalking(){
        this.capture.enabled = false;
        for(let id in this.peers){
            if(this.peers[id].channel) this.peers[id].channel.send('{"type": "stoptalking"}');
        }
    }

    _addChatMessage(username, message){
        const chatMessage = document.createElement('tr');
        const chatName = chatMessage.insertCell()
        chatName.textContent = username;
        chatName.className = 'chatname';
        chatMessage.insertCell().textContent = message;
        this.chatTable.appendChild(chatMessage);
        this.chatTable.scroll(0, this.chatTable.scrollHeight);
    }

    sendChat(message){
        this._addChatMessage(this.username, message);
        const jsonMsg = JSON.stringify({
            type: 'chat',
            message
        });
        for(let id in this.peers){
            if(this.peers[id].channel) this.peers[id].channel.send(jsonMsg);
        }
    }

    close(){
        this.socket.close();

        disconnectSound.currentTime = 0;
        if(!connectSound.playing) disconnectSound.play();

        for(let id in this.peers){
            if(this.peers[id].channel) this.peers[id].channel.send('{"type": "goodbye"}');
            this._userLeft(id);
        }
    }
}
