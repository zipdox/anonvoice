const settingEcho = document.getElementById('setting_echo');
const settingNoise = document.getElementById('setting_noise');
const settingGain = document.getElementById('setting_gain');
const settingMono = document.getElementById('setting_mono');

const roomField = document.getElementById('room');
const usernameField = document.getElementById('username');
const connectForm = document.getElementById('connection');
const talkButton = document.getElementById('talk');
const statusLabel = document.getElementById('status');
const userList = document.getElementById('userlist');
const callBox = document.getElementById('call');
const disconnectButton = document.getElementById('disconnect');

const joinSound = document.getElementById('joinsound');
const leaveSound = document.getElementById('leavesound');
const reconnectSound = document.getElementById('reconnectsound');
const connectSound = document.getElementById('connectsound');
const disconnectSound = document.getElementById('disconnectsound');

const chatMessages = document.getElementById('chatmessages');
const chatForm = document.getElementById('chatform');
const chatInput = document.getElementById('chatmessage');

let context;
let finalNode
let sendStream;

async function initAudio(){
    // create an audio context to output to
    context = new AudioContext();
    finalNode = new MediaStreamAudioDestinationNode(context, {channelCount: 2});
    sendStream = finalNode.stream.getAudioTracks()[0];
    sendStream.enabled = false;

    // get the mic stream
    const mediaStream = await navigator.mediaDevices.getUserMedia({audio: {echoCancellation: settingEcho.checked, autoGainControl: settingGain.checked, noiseSuppression: settingNoise.checked}, video: false});
    console.log(mediaStream);

    // audioTrack for changing constraints
    const audioTrack = mediaStream.getAudioTracks()[0];
    settingEcho.onclick = settingNoise.onclick = settingGain.onclick = function(){
        audioTrack.applyConstraints({echoCancellation: settingEcho.checked, autoGainControl: settingGain.checked, noiseSuppression: settingNoise.checked});
    }

    // create a MediaStreamAudioSourceNode from the mic stream, this doesn't start playing the stream though
    const source = context.createMediaStreamSource(mediaStream);
    console.log(source);

    let processingSource;

    // Processing for mono
    const monoMerger = new ChannelMergerNode(context, {numberOfInputs: source.channelCount, channelCount: 1});
    const monoSplitter = new ChannelSplitterNode(context, {numberOfOutputs: 2});
    source.connect(monoMerger);
    monoMerger.connect(monoSplitter);

    // Processing for stereo
    let stereoSource;
    if(source.channelCount > 1){
        stereoSource = source;
    }else{
        stereoSource = new ChannelSplitterNode(context, {channelCount: source.channelCount, numberOfOutputs: 2});
        source.connect(stereoSource);
    }

    settingMono.onchange = function(){
        if(processingSource != undefined) processingSource.disconnect(finalNode);
        if(settingMono.checked){
            processingSource = monoSplitter;
        }else{
            processingSource = stereoSource;
        }
        processingSource.connect(finalNode);
    }

    settingMono.onchange();
}

let client;
connectForm.onsubmit = async function(event){
    event.preventDefault();
    await initAudio();
    client = new VoiceClient(roomField.value, usernameField.value, sendStream, statusLabel, userList, chatMessages);
    callBox.removeAttribute('hidden');
    this.setAttribute('hidden', '');
}

disconnectButton.onclick = function(){
    if(client != undefined) client.close();
    connectForm.removeAttribute('hidden');
    callBox.setAttribute('hidden', '');
    talkButton.style.color = null;
    talkButton.style.fontWeight = null;
}
 
talkButton.onmousedown = function(){
    client.startTalking();
    this.style.color = 'red';
    this.style.fontWeight = 'bold';
}
talkButton.ontouchstart = talkButton.onmousedown;

talkButton.onmouseup = function(){
    client.stopTalking();
    this.style.color = null;
    this.style.fontWeight = null;
}
talkButton.ontouchend = talkButton.onmouseup;

chatForm.onsubmit = function(event){
    event.preventDefault();
    const text = chatInput.value;
    if(text == '') return;
    chatInput.value = '';
    if(client == undefined) return;
    client.sendChat(text);
}

window.onbeforeunload = function(){
    if(client != undefined) client.close();
    return null;
}
